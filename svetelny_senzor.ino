//Světelný senzor
// *Malá hodnota -> světlo
// *Velká hodnota -> tma

int Sensor = A0;
int sensorValue;

void setup() {
  pinMode(Sensor, INPUT); //Senzor světla -> A0
  Serial.begin(9600);
}

void loop() {
  sensorValue = analogRead(Sensor); //Senzor světla - Načtení hodnoty senzoru
  Serial.println(sensorValue, DEC); //Senzor světla - Výpis do konzole
}