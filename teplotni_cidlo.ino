// Teplotní čidlo DS18B20
#include <OneWire.h>
#include <DallasTemperature.h>

const int teplotniCidlo = 36; //Pin
OneWire oneWire(teplotniCidlo);
DallasTemperature senzory(&oneWireDS);

void setup(void) {
  Serial.begin(9600);
  // Aktivace komunikace s knihovnou
  senzory.begin();
}

void loop(void) {
  // načtení informací ze všech připojených čidel na daném pinu
  senzoryDS.requestTemperatures();
  // výpis teploty na sériovou linku, při připojení více čidel
  // pomocí změny čísla v závorce (0) - adresa čidla
  Serial.print("Teplota cidla: ");
  Serial.print(senzoryDS.getTempCByIndex(0));
  Serial.println("C");
}